use iced::widget::image;
use reqwest::Client;
use serde::Deserialize;
use serde_json::json;

const ANILIST_API: &str = "https://graphql.anilist.co/";
const ANIME_QUERY: &str = "
query ($id: Int) { # Define which variables will be used in the query (id)
  Media (id: $id, type: ANIME) { # Insert our variables into the query arguments (id) (type: ANIME is hard-coded in the query)
    id
    title {
      romaji
      english
      native
    },
    description
    status,
    isAdult
    startDate {
      year
      month
      day
    },
    endDate {
      year
      month
      day
    },
    averageScore,
    format,
    episodes,
    siteUrl,
    coverImage {
      extraLarge
      large
      medium
      color
    },
    nextAiringEpisode {
      id
      episode
      timeUntilAiring
      airingAt
    }
  }
}
";

const SEARCH_QUERY: &str = "
query ($id: Int, $page: Int, $perPage: Int, $search: String) {
  Page(page: $page, perPage: $perPage) {
    pageInfo {
      total
      currentPage
      lastPage
      hasNextPage
      perPage
    }
    media(id: $id, search: $search) {
      id
      title {
        romaji
        english
        native
      }
      siteUrl
      coverImage {
        extraLarge
        large
        medium
        color
      }
    }
  }
}
";

#[derive(Debug, Clone, Deserialize)]
pub struct AnimeDate {
    pub year: i32,
    pub month: i32,
    pub day: i32,
}

#[derive(Debug, Clone, Deserialize)]
pub struct AnimeTitle {
    pub romaji: Option<String>,
    pub english: Option<String>,
    pub native: Option<String>,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CoverImage {
    pub extra_large: Option<String>,
    pub large: Option<String>,
    pub medium: Option<String>,
    pub color: Option<String>,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct NextAiringEpisode {
    pub id: i32,
    pub episode: i32,
    pub time_until_airing: i32,
    pub airing_at: i32,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Anime {
    pub id: i32,
    pub title: AnimeTitle,
    pub description: String,
    pub status: String,
    pub is_adult: bool,
    pub start_date: AnimeDate,
    pub end_date: AnimeDate,
    pub average_score: i32,
    pub format: String,
    pub episodes: i32,
    pub site_url: String,
    pub cover_image: CoverImage,
    pub next_airing_episode: Option<NextAiringEpisode>,
}

#[derive(Debug, Clone)]
pub struct AnimeWithImage {
    pub anime: Anime,
    pub cover_image: image::Handle,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PageInfo {
    pub total: i32,
    pub current_page: i32,
    pub last_page: i32,
    pub has_next_page: bool,
    pub per_page: i32,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MiniAnimeInfo {
    pub id: i32,
    pub title: AnimeTitle,
    pub site_url: Option<String>,
    pub cover_image: CoverImage,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SearchResultAnime {
    pub page_info: PageInfo,
    pub media: Vec<MiniAnimeInfo>,
}

#[derive(Debug, Clone, Deserialize)]
enum AniListTypes {
    Media(Anime),
    Page(SearchResultAnime),
}

#[derive(Debug, Clone, Deserialize)]
struct GraphQLResponse {
    data: AniListTypes,
}

#[derive(Debug, Clone)]
pub enum AniListRequestError {
    APIError,
}

impl From<reqwest::Error> for AniListRequestError {
    fn from(error: reqwest::Error) -> AniListRequestError {
        dbg!(error);

        AniListRequestError::APIError
    }
}

pub async fn get_anime(id: i32) -> Result<AnimeWithImage, AniListRequestError> {
    let client = Client::new();
    let json = json!({
        "query": ANIME_QUERY,
        "variables": {
            "id": id
        }
    });
    let resp = client
        .post(ANILIST_API)
        .json(&json)
        .send()
        .await?
        .json::<GraphQLResponse>()
        .await?;

    let anime = match resp.data {
        AniListTypes::Media(ani_res) => ani_res,
        _ => return Err(AniListRequestError::APIError),
    };

    let cover_image = {
        let image_bytes = reqwest::get(anime.cover_image.medium.clone().unwrap())
            .await?
            .bytes()
            .await?;

        image::Handle::from_memory(image_bytes)
    };

    return Ok(AnimeWithImage { anime, cover_image });
}

pub async fn search_anime(search_term: String) -> Result<SearchResultAnime, AniListRequestError> {
    let client = Client::new();
    let json = json!({
        "query": SEARCH_QUERY,
        "variables": {
            "search": search_term,
            "page": 1,
            "perPage": 10
        }
    });

    let resp = client
        .post(ANILIST_API)
        .json(&json)
        .send()
        .await?
        .json::<GraphQLResponse>()
        .await?;

    let search_results = match resp.data {
        AniListTypes::Page(page_res) => page_res,
        _ => return Err(AniListRequestError::APIError),
    };

    return Ok(search_results);
}
