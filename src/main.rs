use anilist::{search_anime, AniListRequestError, AnimeWithImage, SearchResultAnime};
use iced::widget::{button, column, container, image, row, text, text_input, Button, Row, Text};
use iced::{Alignment, Application, Command, Element, Length, Settings, Theme};
use iced_aw::{Icon, ICON_FONT};

use anilist::get_anime;

use search_result_item::search_result_item;

mod anilist;

pub fn main() -> iced::Result {
    let mut custom_settings = Settings::default();
    custom_settings.window.min_size = Some((450, 200));
    AniIced::run(custom_settings)
}

#[derive(Debug, Clone)]
pub enum Message {
    GetAnime(Option<i32>),
    SearchMedia,
    FoundAnime(Result<AnimeWithImage, AniListRequestError>),
    FoundSearchResults(Result<SearchResultAnime, AniListRequestError>),
    AnimeSearchChanged(String),
}

#[derive(Debug, Clone)]
pub enum AniIced {
    Init { search: String },
    Loading,
    MediaLoaded { anime_with_image: AnimeWithImage },
    MediaSearchResults { search_results: SearchResultAnime },
    Errored,
}

impl Application for AniIced {
    type Message = Message;
    type Theme = Theme;
    type Executor = iced::executor::Default;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, Command<Self::Message>) {
        (
            AniIced::Init {
                search: String::from(""),
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        format!("Anime")
    }

    fn update(&mut self, message: Self::Message) -> Command<Message> {
        match message {
            Message::GetAnime(Some(media_id)) => {
                println!("trying to get anime info");

                *self = AniIced::Loading;

                Command::perform(get_anime(media_id), Message::FoundAnime)
            }
            Message::SearchMedia => {
                let search_value = if let AniIced::Init { search } = self {
                    search
                } else {
                    return Command::none();
                };
                Command::perform(
                    search_anime(search_value.clone()),
                    Message::FoundSearchResults,
                )
            }
            Message::FoundAnime(Ok(anime)) => {
                println!("{:?}", anime);

                *self = AniIced::MediaLoaded {
                    anime_with_image: anime,
                };

                Command::none()
            }
            Message::FoundAnime(Err(_err)) => {
                println!("Most likely an error");

                Command::none()
            }
            Message::AnimeSearchChanged(value) => {
                if let AniIced::Init { search } = self {
                    *search = value
                }

                Command::none()
            }
            Message::FoundSearchResults(Ok(results)) => {
                println!("{:?}", results);

                *self = AniIced::MediaSearchResults {
                    search_results: results,
                };

                Command::none()
            }
            _ => Command::none(),
        }
    }

    fn view(&self) -> Element<'_, Self::Message> {
        let main_content = match self {
            AniIced::Init { search } => {
                column![
                    text_input("Search/Lookup Anime", search).on_input(Message::AnimeSearchChanged),
                    button("Search Anime").on_press(Message::SearchMedia),
                ]
            }
            AniIced::Loading => {
                column![text("Loading...")]
            }
            AniIced::MediaLoaded { anime_with_image } => {
                let cover_image = image::viewer(anime_with_image.cover_image.clone());
                let anime_user_status_button = button("Watching");
                let anime_user_fav_button = Button::new(
                    Text::new(Icon::Heart.to_string())
                        .width(Length::Shrink)
                        .height(Length::Shrink)
                        .font(ICON_FONT),
                );
                column![
                    row![
                        cover_image.height(240),
                        column![
                            text(
                                anime_with_image
                                    .anime
                                    .title
                                    .english
                                    .clone()
                                    .unwrap_or_default()
                            )
                            .size(40),
                            text("Anime Information"),
                            row![anime_user_status_button, anime_user_fav_button].spacing(10)
                        ]
                        .spacing(8)
                    ]
                    .spacing(32)
                    .align_items(Alignment::Center),
                    text(&anime_with_image.anime.description)
                ]
            }
            AniIced::MediaSearchResults { search_results } => {
                let list_media_items: Vec<_> = search_results
                    .media
                    .iter()
                    .clone()
                    .map(|media| search_result_item(media.clone(), Message::GetAnime))
                    .map(Element::from)
                    .collect();

                column(list_media_items).align_items(Alignment::Center)
            }
            AniIced::Errored => {
                column![text("Error")]
            }
        };

        container(main_content.spacing(16).max_width(1280))
            .width(Length::Fill)
            .height(Length::Fill)
            .center_y()
            .center_x()
            .padding(16)
            .into()
    }
}

mod search_result_item {
    use crate::anilist::MiniAnimeInfo;
    use iced::widget::{button, component, row, text, Component};
    use iced::{Alignment, Element, Renderer};

    pub struct SearchResultItem<Message> {
        media: MiniAnimeInfo,
        on_click: Box<dyn Fn(Option<i32>) -> Message>,
    }

    pub fn search_result_item<Message>(
        media: MiniAnimeInfo,
        on_click: impl Fn(Option<i32>) -> Message + 'static,
    ) -> SearchResultItem<Message> {
        SearchResultItem::new(media, on_click)
    }

    impl<Message> SearchResultItem<Message> {
        pub fn new(
            media: MiniAnimeInfo,
            on_click: impl Fn(Option<i32>) -> Message + 'static,
        ) -> Self {
            Self {
                media,
                on_click: Box::new(on_click),
            }
        }
    }

    #[derive(Clone)]
    pub enum Event {
        MediaSelected,
    }

    impl<Message> Component<Message, Renderer> for SearchResultItem<Message> {
        type State = ();
        type Event = Event;

        fn update(&mut self, state: &mut Self::State, event: Self::Event) -> Option<Message> {
            match event {
                Event::MediaSelected => Some((self.on_click)(Some(self.media.id))),
            }
        }
        fn view(&self, _state: &Self::State) -> Element<Event, Renderer> {
            let title = match self.media.title.english.clone() {
                Some(val) => val,
                None => self
                    .media
                    .title
                    .native
                    .clone()
                    .unwrap_or(String::from("unknown")),
            };
            row![button(text(title)).on_press(Event::MediaSelected),]
                .align_items(Alignment::Center)
                .spacing(10)
                .into()
        }
    }

    impl<'a, Message> From<SearchResultItem<Message>> for Element<'a, Message, Renderer>
    where
        Message: 'a,
    {
        fn from(search_result_item: SearchResultItem<Message>) -> Self {
            component(search_result_item)
        }
    }
}
